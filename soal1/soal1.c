#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>

#define BUFF_SIZE 1024
#define THREAD_COUNT 10

pthread_t tid[THREAD_COUNT]; // inisialisasi array untuk menampung thread dalam kasus ini ada 2 thread
pid_t child;
DIR *dp;
struct dirent *ep;

/* ---- Base64 Encoding/Decoding Table --- */
char b64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/* decodeblock - decode 4 '6-bit' characters into 3 8-bit binary bytes */
void decodeblock(unsigned char in[], char *clrstr)
{
    unsigned char out[4];
    out[0] = in[0] << 2 | in[1] >> 4;
    out[1] = in[1] << 4 | in[2] >> 2;
    out[2] = in[2] << 6 | in[3] >> 0;
    out[3] = '\0';
    strncat(clrstr, out, sizeof(out));
}

void b64_decode(char *b64src, char *clrdst)
{
    int c, phase, i;
    unsigned char in[4];
    char *p;

    clrdst[0] = '\0';
    phase = 0;
    i = 0;
    while (b64src[i])
    {
        c = (int)b64src[i];
        if (c == '=')
        {
            decodeblock(in, clrdst);
            break;
        }
        p = strchr(b64, c);
        if (p)
        {
            in[phase] = p - b64;
            phase = (phase + 1) % 4;
            if (phase == 0)
            {
                decodeblock(in, clrdst);
                in[0] = in[1] = in[2] = in[3] = 0;
            }
        }
        i++;
    }
}

void *playandcount(void *arg)
{
    pthread_t id = pthread_self();
    int iter;
    if (pthread_equal(id, tid[0])) // thread untuk mkdir music
    {
        child = fork();
        if (child == 0)
        {
            char *argv1[] = {"mkdir", "-p", "music", NULL};
            execv("/bin/mkdir", argv1);
        }
    }
    else if (pthread_equal(id, tid[1])) // thread untuk unzip music
    {
        child = fork();
        if (child == 0)
        {
            char *argv2[] = {"unzip", "music.zip", "-d", "music", NULL};
            execv("/bin/unzip", argv2);
        }
    }
    else if (pthread_equal(id, tid[2])) // thread untuk mkdir quote
    {
        child = fork();
        if (child == 0)
        {
            char *argv3[] = {"mkdir", "-p", "quote", NULL};
            execv("/bin/mkdir", argv3);
        }
    }
    else if (pthread_equal(id, tid[3])) // thread untuk unzip quote
    {
        child = fork();
        if (child == 0)
        {
            char *argv4[] = {"unzip", "quote.zip", "-d", "quote", NULL};
            execv("/bin/unzip", argv4);
        }
    }
    else if (pthread_equal(id, tid[4])) // thread untuk proses dir music
    {
		sleep(1);
        child = fork();
        if (child == 0)
        {
            char path[100] = "./music";
            dp = opendir(path);
            if (dp != NULL)
            {
                while (ep = readdir(dp))
                {
                    if (strstr(ep->d_name, "txt"))
                    {
                        char oldFilePath[100] = "./music/";
                        char newFilePath[100] = "./music.txt";
                        strcat(oldFilePath, ep->d_name);

                        FILE *oriFile, *newFile;
                        oriFile = fopen(oldFilePath, "r+");
                        newFile = fopen(newFilePath, "a+");

                        char b64[BUFF_SIZE], decoded[BUFF_SIZE];
                        fgets(b64, BUFF_SIZE, oriFile);
                        b64_decode(b64, decoded);
                        fputs(decoded, newFile);
                        fputc('\n', newFile);

                        fclose(oriFile);
                        fclose(newFile);
                    }
                }
            }
        }
    }
    else if (pthread_equal(id, tid[5])) // thread untuk proses dir quote
    {
		sleep(1);
        child = fork();
        if (child == 0)
        {
            char path[100] = "./quote";
            dp = opendir(path);
            if (dp != NULL)
            {
                while (ep = readdir(dp))
                {
                    if (strstr(ep->d_name, "txt"))
                    {
                        char oldFilePath[100] = "./quote/";
                        char newFilePath[100] = "./quote.txt";
                        strcat(oldFilePath, ep->d_name);

                        FILE *oriFile, *newFile;
                        oriFile = fopen(oldFilePath, "r+");
                        newFile = fopen(newFilePath, "a+");

                        char b64[BUFF_SIZE], decoded[BUFF_SIZE];
                        fgets(b64, BUFF_SIZE, oriFile);
                        b64_decode(b64, decoded);
                        fputs(decoded, newFile);
                        fputc('\n', newFile);

                        fclose(oriFile);
                        fclose(newFile);
                    }
                }
            }
        }
    }
    else if (pthread_equal(id, tid[6])) // thread untuk mkdir hasil
    {
        child = fork();
        if (child == 0)
        {
            char *argv5[] = {"mkdir", "-p", "hasil", NULL};
            execv("/bin/mkdir", argv5);
        }
    }
    else if (pthread_equal(id, tid[7])) // thread untuk move quote dan music ke hasil
    {
		sleep(2);
        char *argv6[] = {"mv", "-t", "./hasil", "quote.txt", "music.txt", NULL};
		execv("/bin/mv", argv6);
    }
    else if (pthread_equal(id, tid[8])) // thread untuk zip file
    {
		sleep(3);
        char *argv7[] = {"zip", "-r", "hasil.zip", "./hasil", "-P", "mihinomenestricho", NULL};
		execv("/bin/zip", argv7);
    }
    else // thread untuk mengatakan nice
    {
        printf("thread terakhir\n");
        printf("Nice\n");
        pthread_exit(NULL);
    }
    return NULL;
}

int main(void)
{
    int i = 0;
    int err;
    while (i < THREAD_COUNT) // loop sejumlah thread
    {
        err = pthread_create(&(tid[i]), NULL, &playandcount, NULL); // membuat thread
        if (err != 0)                                               // cek error
        {
            printf("\n can't create thread : [%s]", strerror(err));
        }
        else
        {
            printf("create thread success %d\n", i);
        }
        i++;
    }

    i = 0;
    while (i < THREAD_COUNT - 1) // loop sejumlah thread
    {
        pthread_join(tid[i], NULL);
        i++;
    }
    exit(0);
    return 0;
}