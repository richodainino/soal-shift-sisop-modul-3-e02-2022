#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

void writefile(int socketfd){
    int n;
    FILE *f = fopen("hartakarun.zip","wb");
    char buffer[1024];
    while(1){
        n = recv(socketfd, buffer, 1024, 0);
        if(n <= 0){
            return;
        }
        fwrite(buffer, sizeof(char), n, f);
        bzero(buffer, 1024);
    }
}

int main(){
    char *ip = "127.0.0.1";
    int port = 8080;
    int socketfd, new_socket;
    struct sockaddr_in server_addr, new_addr;
    char buffer[1024];

    socketfd = socket(AF_INET, SOCK_STREAM, 0);

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = port;
    server_addr.sin_addr.s_addr = inet_addr(ip);

    int binder = bind(socketfd, (struct sockaddr*)&server_addr, sizeof(server_addr));
    if(binder < 0){
        printf("Binding error\n");
        exit(1);
    }
    if(listen(socketfd, 10) != 0){
        printf("Listening error\n");
        exit(1);
    }
    socklen_t addrsize = sizeof(new_addr);
    new_socket = accept(socketfd, (struct sockaddr*)&new_addr, &addrsize);
    writefile(new_socket);
    printf("File Received\n");

    return 0;
}