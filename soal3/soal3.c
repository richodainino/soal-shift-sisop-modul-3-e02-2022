#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <unistd.h>

void *move(void *filename){
    char cwd[PATH_MAX];
    char dirname[200], file[100];
    strcpy(file, filename);
    char *shortname = strrchr(filename, '/');
    while(strchr(shortname+1, '/') != NULL) shortname = strchr(shortname+1, '/');

    if (shortname[1] == '.'){
        strcpy(dirname, "Hidden");
    }

    else if (strstr(filename, ".") != NULL){
        char *formatname = strchr(shortname, '.');

        for (int i = 1; i < strlen(formatname); i++){
            formatname[i] = tolower(formatname[i]);
        }
        strcpy(dirname, formatname+1);
    }
    
    else{
        strcpy(dirname, "Unknown");
    }
    //printf("%s\n", file);
    //printf("%s\n", dirname);
    mkdir(dirname, 0777);
    if(strcmp(dirname, "_") == 0){
        mkdir("_", 0777);
        //printf("YOWW");
    }

    if (getcwd(cwd, sizeof(cwd)) != NULL){
        char namafile[200];
        strcpy(namafile, cwd);
        strcat(namafile, "/");
        strcat(namafile, dirname);
        strcat(namafile, "/");
        strcat(namafile, shortname+1);

        rename(file, namafile);
    }
}

void listFilesRecursively(char *basePath){
    char path[1000];
    struct dirent *dp;
    struct stat fs;
    DIR *dir = opendir(basePath);
    int n = 0;

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL){
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);
            int tmp = stat(path, &fs);
            if (tmp == 0 && S_ISREG(fs.st_mode)){
                pthread_t thread;
                int err = pthread_create(&thread, NULL, move, (void *)path);
                pthread_join(thread, NULL);
            }

            listFilesRecursively(path);
            if(!S_ISREG(fs.st_mode)) remove(path);
        }
    }
    closedir(dir);
}

int main(){
    char cwd[PATH_MAX];
    getcwd(cwd, sizeof(cwd)); // /home/haniif/shift3/hartakarun
    //printf("ok\n");
    //printf("%s\n", cwd);
    listFilesRecursively(cwd);
    return 0;
}