#include <stdio.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

void sendfile(FILE *f, int socketfd){
    int n;
    char data[1024];
    bzero(data, 1024);
    int filesize = 0;
    while((filesize = fread(data, sizeof(char),1024,f)) > 0){
        send(socketfd, data, sizeof(data), 0);
    }
    bzero(data, 1024);
}

int main(){
    int status, id = fork();
    rename("../hartakarun", "hartakarun");
    if(id == 0){
        char *argv[] = {"zip", "-rm", "hartakarun.zip", "hartakarun"};
        execv("/bin/zip",argv);
    }
    else while(wait(&status) > 0);

    char *ip = "127.0.0.1";
    int port = 8080;
    int valread, socketfd;
    struct sockaddr_in server_addr;
    socketfd = socket(AF_INET, SOCK_STREAM, 0);

    server_addr.sin_addr.s_addr = inet_addr(ip);
    server_addr.sin_port = port;
    server_addr.sin_family = AF_INET;

    int connection = connect(socketfd, (struct sockaddr*)&server_addr, sizeof(server_addr));

    char commands[1024];
    int loop = 1;
    while(loop){
        bzero(commands, 1024);
        scanf(" %[^\n]s", commands);
        printf("%s\n", commands);
        write(socketfd, commands, strlen(commands));
        if(strcmp(commands, "send hartakarun.zip")==0){
            FILE *f = fopen("hartakarun.zip", "r");
            sendfile(f, socketfd);
            loop = 0;
            printf("File sent successfully\n");
        }
        else printf("Unknown Command\n");
    }
    close(socketfd);
}
