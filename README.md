# Sisop soal shift modul 3 kelompok E02 2022
Anggota:
1. Haniif Ahmad Jauhari | 5025201224
2. Made Rianja Richo Dainino | 5025201236
3. Dicky Indra Kuncahyo | 5025201250

## Soal 1
Dikerjakan oleh Made Rianja Richo Dainino | 5025201236

Pada soal 1.a kita diminta untuk unzip 2 file ke dalam 2 folder yang berbeda. Kita dapat menyelesaikan hal ini dengan menggunakan `thread` dan melakukan `exec mkdir` lalu `exec unzip` sebagai berikut

```c
if (pthread_equal(id, tid[0])) // thread untuk mkdir music
{
   child = fork();
   if (child == 0)
   {
      char *argv1[] = {"mkdir", "-p", "music", NULL};
      execv("/bin/mkdir", argv1);
   }
}
else if (pthread_equal(id, tid[1])) // thread untuk unzip music
{
   child = fork();
   if (child == 0)
   {
      char *argv2[] = {"unzip", "music.zip", "-d", "music", NULL};
      execv("/bin/unzip", argv2);
   }
}
else if (pthread_equal(id, tid[2])) // thread untuk mkdir quote
{
   child = fork();
   if (child == 0)
   {
      char *argv3[] = {"mkdir", "-p", "quote", NULL};
      execv("/bin/mkdir", argv3);
   }
}
else if (pthread_equal(id, tid[3])) // thread untuk unzip quote
{
   child = fork();
   if (child == 0)
   {
      char *argv4[] = {"unzip", "quote.zip", "-d", "quote", NULL};
      execv("/bin/unzip", argv4);
   }
}
```

Pada soal 1.b kita diminta untuk mendecode semua file .txt dengan base64 dan memasukkan hasilnya ke dalam satu file .txt yang baru di masing masing folder. Kita dapat menggunakan directory listing yang didapatkan pada modul sebelumnya, di mana pada tiap file kita melakukan decode base64 menggunakan fungsi yang saya temukan pada [Google](https://fm4dd.com/programming/base64/base64_stringencode_c.shtm). Fungsi tersebut akan mendecode base64 dan memasukkan nilainya ke dalam variabel string yang dibawa pada parameter. Variabel string ini akan ditaruh pada file yang baru menggunakan `fputs`. Kodenya sebagai berikut


```c
else if (pthread_equal(id, tid[4])) // thread untuk proses dir music
{
sleep(1);
   child = fork();
   if (child == 0)
   {
      char path[100] = "./music";
      dp = opendir(path);
      if (dp != NULL)
      {
            while (ep = readdir(dp))
            {
               if (strstr(ep->d_name, "txt"))
               {
                  char oldFilePath[100] = "./music/";
                  char newFilePath[100] = "./music.txt";
                  strcat(oldFilePath, ep->d_name);

                  FILE *oriFile, *newFile;
                  oriFile = fopen(oldFilePath, "r+");
                  newFile = fopen(newFilePath, "a+");

                  char b64[BUFF_SIZE], decoded[BUFF_SIZE];
                  fgets(b64, BUFF_SIZE, oriFile);
                  b64_decode(b64, decoded);
                  fputs(decoded, newFile);
                  fputc('\n', newFile);

                  fclose(oriFile);
                  fclose(newFile);
               }
            }
      }
   }
}
else if (pthread_equal(id, tid[5])) // thread untuk proses dir quote
{
sleep(1);
   child = fork();
   if (child == 0)
   {
      char path[100] = "./quote";
      dp = opendir(path);
      if (dp != NULL)
      {
            while (ep = readdir(dp))
            {
               if (strstr(ep->d_name, "txt"))
               {
                  char oldFilePath[100] = "./quote/";
                  char newFilePath[100] = "./quote.txt";
                  strcat(oldFilePath, ep->d_name);

                  FILE *oriFile, *newFile;
                  oriFile = fopen(oldFilePath, "r+");
                  newFile = fopen(newFilePath, "a+");

                  char b64[BUFF_SIZE], decoded[BUFF_SIZE];
                  fgets(b64, BUFF_SIZE, oriFile);
                  b64_decode(b64, decoded);
                  fputs(decoded, newFile);
                  fputc('\n', newFile);

                  fclose(oriFile);
                  fclose(newFile);
               }
            }
      }
   }
}
```

Pada soal 1.c kita diminta untuk memindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil. Dapat dilakukan dengan `exec mkdir` dan `exec mv` sebagai berikut

```c
else if (pthread_equal(id, tid[6])) // thread untuk mkdir hasil
{
   child = fork();
   if (child == 0)
   {
      char *argv5[] = {"mkdir", "-p", "hasil", NULL};
      execv("/bin/mkdir", argv5);
   }
}
else if (pthread_equal(id, tid[7])) // thread untuk move quote dan music ke hasil
{
sleep(2);
   char *argv6[] = {"mv", "-t", "./hasil", "quote.txt", "music.txt", NULL};
execv("/bin/mv", argv6);
}
```

Pada soal 1.d kita diminta untuk zip folder hasil dengan password 'mihinomenest[Nama user]'. Dalam kasus ini saya menggunakan 'mihinomenestricho' sebagai password dan melakukan zip-nya dengan melakukan `exec zip`

```c
else if (pthread_equal(id, tid[8])) // thread untuk zip file
{
sleep(3);
   char *argv7[] = {"zip", "-r", "hasil.zip", "./hasil", "-P", "mihinomenestricho", NULL};
execv("/bin/zip", argv7);
}
```

Pada soal 1.e kita dimin ta untuk unzip file hasil.zip tadi dan membuat file no.txt dengan tulisan 'No' dan lalu kita zip kembali folder tersebut. Kita dapat melakukannya dengan `exec unzip` lalu membuat file baru dan kembali melakukan `exec zip` seperti sebelumnya. Untuk melakukannya secara serentak kita dapat menggunakan thread. Untuk soal pada bagian ini saya mengalami kendala dengan urutan thread, sehingga saya tidak dapat memberi kode langsung implementasinya.

## Soal 2
Dikerjakan oleh Dicky Indra Kuncahyo | 5025201250

 <h6> soal2 <h6>

manfaatkan thread untuk dapat memungkinkan terjadinya >1 koneksi Client ke Server dengan cara menyimpan jumlah Client yang sedang terkoneksi pada suatu variabel dan tentukan apakah Client akan di terima atau di tahan aksesnya ke fungsi program berdasarkan jumlah koneksi
    
    while(true) {
        if ((new_socket = accept(server_fd, 
            (struct sockaddr *) &address, (socklen_t*) &addr_len)) < 0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }
            pthread_create(&(tid[total]), NULL, &client, &new_socket);
                total++;
            

        if (total == 1) {
            send(new_socket, hello, STR_SIZE, 0);
        }
        else {
            send(new_socket, deny, STR_SIZE, 0);
        }
    }

Untuk servernya menghandle Client yang masuk dan menggunakan while apabila client >1 agar menunggu
    while (!band(hello, buffer))

    while (total > 1) {
        valread = read(new_socket, buffer, STR_SIZE);
        if (total == 1) {
         send(new_socket, hello, STR_SIZE, 0);
        }
        else {
            send(new_socket, deny, STR_SIZE, 0);
        }
    }

Ketika client memutuskan koneksi decrement jumlah koneksi
    else if (equal(quit, buffer)) {
     close(new_socket);
     total--;
        break;
    }

Fungsi register

Client mengirim command ke server, dan server menerima serta menyiapkan akun.txt . Disini client-server akan melakukan serah terima data username dan password untuk disimpan di akun.txt

    fp2 = fopen("akun.txt", "a");
    fprint
    close

Client mengirim file ke server dan server akan menyimpannya

    int fd = open(data, O_RDONLY);
        if (!fd) {
        perror("can't open");
        exit(EXIT_FAILURE);
        }

        int read_len;
    while (true) {
        memset(data, 0x00, STR_SIZE);
        read_len = read(fd, data, STR_SIZE);

        if (read_len == 0) {
            break;
            }
        else {
        send(sock, data, read_len, 0);                               
        }
    }
        close(fd);

    int des_fd = open(request.path, O_WRONLY | O_CREAT | O_EXCL, 0700);
        if (!des_fd) {
        perror("can't open file");
        exit(EXIT_FAILURE);
        }

Penambahan detail di file.tsv

    valread = read(new_socket, request.judul, STR_SIZE);
    valread = read(new_socket, request.description, STR_SIZE);
    valread = read(new_socket, clientPath, STR_SIZE);

Solusi download file hampir sama dengan command add, hanya saja peran client-server dibalik

## Soal 3
Dikerjakan oleh Haniif Ahmad Jauhari | 5025201224

<h6> soal3.c </h6>

Pada file ini, digunakan dua fungsi, yakni `move` dan `listFilesRecursively`.

Berikut potongan kode fungsi `move`.

    void *move(void *filename){
        char cwd[PATH_MAX];
        char dirname[200], file[100];
        strcpy(file, filename);
        char *shortname = strrchr(filename, '/');
        while(strchr(shortname+1, '/') != NULL) shortname = strchr(shortname+1, '/');

        if (shortname[1] == '.'){
            strcpy(dirname, "Hidden");
        }

        else if (strstr(filename, ".") != NULL){
            char *formatname = strchr(shortname, '.');

            for (int i = 1; i < strlen(formatname); i++){
                formatname[i] = tolower(formatname[i]);
            }
            strcpy(dirname, formatname+1);
        }

        else{
            strcpy(dirname, "Unknown");
        }
        //printf("%s\n", file);
        //printf("%s\n", dirname);
        mkdir(dirname, 0777);
        if(strcmp(dirname, "_") == 0){
            mkdir("_", 0777);
            //printf("YOWW");
        }

        if (getcwd(cwd, sizeof(cwd)) != NULL){
            char namafile[200];
            strcpy(namafile, cwd);
            strcat(namafile, "/");
            strcat(namafile, dirname);
            strcat(namafile, "/");
            strcat(namafile, shortname+1);

            rename(file, namafile);
        }
    }

Fungsi `move` tersebut digunakan untuk memindah file dari folder hartakarun ke folder kategorinya (sekaligus membuat folder kategorinya jika belum ada).

Berikut potongan kode fungsi `listFilesRecursively`.

    void listFilesRecursively(char *basePath){
        char path[1000];
        struct dirent *dp;
        struct stat fs;
        DIR *dir = opendir(basePath);
        int n = 0;

        if (!dir)
            return;

        while ((dp = readdir(dir)) != NULL){
            if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
                strcpy(path, basePath);
                strcat(path, "/");
                strcat(path, dp->d_name);
                int tmp = stat(path, &fs);
                if (tmp == 0 && S_ISREG(fs.st_mode)){
                    pthread_t thread;
                    int err = pthread_create(&thread, NULL, move, (void *)path);
                    pthread_join(thread, NULL);
                }

                listFilesRecursively(path);
                if(!S_ISREG(fs.st_mode)) remove(path);
            }
        }
        closedir(dir);
    }

Fungsi `listFilesRecursively` digunakan untuk traverse semua file di dalam folder hartakarun secara rekursif. Kemudian, jika ditemukan file regular, maka akan dibuat thread untuk memanggil fungsi move untuk file yang ditemukan. Terakhir, akan dihapus path/folder kosong yang telah di-traverse sebelumnya.

Pada fungsi `main`, kita hanya perlu memanggil fungsi `listFilesRecursively` dengan argumen *current working directory* seperti berikut.

    int main(){
        char cwd[PATH_MAX];
        getcwd(cwd, sizeof(cwd)); // /home/haniif/shift3/hartakarun
        //printf("ok\n");
        //printf("%s\n", cwd);
        listFilesRecursively(cwd);
        return 0;
    }


<h6>server.c</h6>

Pada file ini terdapat fungsi `writefile` yang digunakan untuk menulis file yang diterima dari client melalui socket.

    void writefile(int socketfd){
        int n;
        FILE *f = fopen("hartakarun.zip","wb");
        char buffer[1024];
        while(1){
            n = recv(socketfd, buffer, 1024, 0);
            if(n <= 0){
                return;
            }
            fwrite(buffer, sizeof(char), n, f);
            bzero(buffer, 1024);
        }
    }

Pada file `server.c` ini, pertama kita buat server socket untuk membuat koneksi dengan client seperti berikut.
    
    char *ip = "127.0.0.1";
    int port = 8080;
    int socketfd, new_socket;
    struct sockaddr_in server_addr, new_addr;
    char buffer[1024];

    socketfd = socket(AF_INET, SOCK_STREAM, 0);

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = port;
    server_addr.sin_addr.s_addr = inet_addr(ip);

Kemudian, kita lakukan binding dan listening seperti berikut.
    
    int binder = bind(socketfd, (struct sockaddr*)&server_addr, sizeof(server_addr));
    if(binder < 0){
        printf("Binding error\n");
        exit(1);
    }
    if(listen(socketfd, 10) != 0){
        printf("Listening error\n");
        exit(1);
    }

Selanjutnya, kita accept permintaan koneksi dari client. Kemudian kita jalankan fungsi `writefile` untuk menuliskan file sesuai dengan yang dikirim oleh client.

    socklen_t addrsize = sizeof(new_addr);
    new_socket = accept(socketfd, (struct sockaddr*)&new_addr, &addrsize);
    writefile(new_socket);
    printf("File Received\n");


<h6>client.c</h6>

Pada file ini terdapat fungsi `sendfile` yang digunakan untuk mengirim data file ke server melalui koneksi socket seperti berikut.

    void sendfile(FILE *f, int socketfd){
        int n;
        char data[1024];
        bzero(data, 1024);
        int filesize = 0;
        while((filesize = fread(data, sizeof(char),1024,f)) > 0){
            send(socketfd, data, sizeof(data), 0);
        }
        bzero(data, 1024);
    }

Pada file `client.c` ini, pertama kita **zip** folder hartakarun kita terlebih dahulu seperti berikut.

    int status, id = fork();
    rename("../hartakarun", "hartakarun");
    if(id == 0){
        char *argv[] = {"zip", "-rm", "hartakarun.zip", "hartakarun"};
        execv("/bin/zip",argv);
    }
    else while(wait(&status) > 0);

Selanjutnya, kita hubungkan koneksi dengan server seperti berikut.

    char *ip = "127.0.0.1";
    int port = 8080;
    int valread, socketfd;
    struct sockaddr_in server_addr;
    socketfd = socket(AF_INET, SOCK_STREAM, 0);

    server_addr.sin_addr.s_addr = inet_addr(ip);
    server_addr.sin_port = port;
    server_addr.sin_family = AF_INET;

    int connection = connect(socketfd, (struct sockaddr*)&server_addr, sizeof(server_addr));

Selanjutnya, kita tunggu command dari user. Jika didapatkan command `send hartakarun.zip`, maka kita jalankan fungsi `sendfile` untuk mengirimkan data file `hartakarun.zip` ke server melalui koneksi socket seperti berikut. Jika selesai, maka kita tutup koneksi socketnya.

    char commands[1024];
    int loop = 1;
    while(loop){
        bzero(commands, 1024);
        scanf(" %[^\n]s", commands);
        printf("%s\n", commands);
        write(socketfd, commands, strlen(commands));
        if(strcmp(commands, "send hartakarun.zip")==0){
            FILE *f = fopen("hartakarun.zip", "r");
            sendfile(f, socketfd);
            loop = 0;
            printf("File sent successfully\n");
        }
        else printf("Unknown Command\n");
    }
    close(socketfd);
